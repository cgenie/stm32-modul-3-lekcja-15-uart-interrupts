/*
 * ring_buffer.c
 *
 *  Created on: Oct 30, 2020
 *      Author: przemek
 */

#include "main.h"
#include "ring_buffer.h"

RbStatus rbRead(Ringbuffer *rb, uint8_t *value) {
	if(rb->head == rb->tail) {
		return RB_ERROR;
	}

	*value = rb->buffer[rb->tail];
	rb->tail = (rb->tail + 1) % RING_BUFFER_SIZE;

	return RB_OK;
}

RbStatus rbWrite(Ringbuffer *rb, uint8_t value) {
	uint8_t tmpHead = (rb->head + 1) % RING_BUFFER_SIZE;

	if(tmpHead == rb->tail) {
		return RB_ERROR;
	}

	rb->buffer[rb->head] = value;
	rb->head = tmpHead;

	return RB_OK;
}
